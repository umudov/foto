<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'foto');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HjFp.TV6T~oLIJsT6]6X{h+?y.3VvDxJw=z p|E/[,mj{S{-m#P4+[KZR*2:Ow> ');
define('SECURE_AUTH_KEY',  '7b0P>K_o&2L|KG;)GU2lZo1RE~uOa+)iS9x_T-rr:%9_V$v@[3q.k%stqQjvn7px');
define('LOGGED_IN_KEY',    'olqfY4cl@&tE`f}@W/z%}r,.2&R`.[Vq!/yE^e_8vMF&6YMPIc1PT:]0.>j2b/oG');
define('NONCE_KEY',        'U*CZ1q49uW[7jC%Q;?qpT>J-`i:sG2`Y,$lfXD<i1v%z3WN^&LS71UUw|P|AW `_');
define('AUTH_SALT',        'vwa-Ri#lr<LckTz1a)*Di:Qgy2<m0cfXBz`<OrxAWfDZpG-_Mr!6=Cf:t 2I1I0^');
define('SECURE_AUTH_SALT', 'v!,KhyD$!MDO+Suh0[W~OcNF}J/%IQPl^O;qasDgp@DuP@Nh3#wfcjGzutnig$3 ');
define('LOGGED_IN_SALT',   'AWVxe+P.o#v)8NsF4>0^,R,24BhTpm02ZlzdX8a5F}M9{eMlA7*hfyM%0d%D{-2q');
define('NONCE_SALT',       'V_ah~|4,wu56?F?-kBG%x,+&!JFQC*p[7v^Su_Jy|6_fwd%VhBp}-y_w*@+&?{Fg');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'njrq_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
