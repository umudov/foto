<?php get_header(); ?>

<h1 style="padding-top: 120px; padding-bottom: 120px; font-family: Poiret One">
    <?php if (!function_exists("qtrans_getSortedLanguages")): ?>
        <!-- Activate qTranslate plugin -->
    <?php else: ?>
        <?php if (qtrans_getLanguage() == "en"): ?> 
            404 page not found!</h1>
    <?php else: ?>
        <?php if (qtrans_getLanguage() == "ru"): ?>
            404 страница не найдена!</h1>
        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?>
<?php get_footer(); ?>