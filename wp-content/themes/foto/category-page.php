<?php

/**
 * Template Name: Category Page
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
get_header();

$page_tree_array = get_pages(array('child_of' => get_the_ID()));
echo "<div class='gallery-'>";
if (count($page_tree_array) > 0) {
    foreach ($page_tree_array as $post) {
        echo "<div class='gallery-preview '>
                    <a href='" . get_page_link(get_the_ID()) . "'>
                            <div class='name'>" . $post->post_title . "</div> 
                            " . get_the_post_thumbnail($post->ID, array(300, 200)) . "
                    </a>
              </div>";
    }
} else {
    echo "Ничего не найдено";
}
echo "</div><div class='clear' style='width:100%; overflow: hidden;'></div>";

get_footer();
