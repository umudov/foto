<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php bloginfo('name'); ?></title>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/main.css">
    </head>

    <body <?php body_class($class); ?>>
        <div id="page-preloader"></div>
        <div class="front-page">
            <div class="wrap">
                <div class="logo"><a href="/foto"><?php bloginfo('name'); ?><sup>©</sup></a></div>
                <div class="links">
                    <a class="left" href="gallery-light">art photo</a>
                    <a href="gallery-dark">art design</a>
                </div>
            </div>
        </div>
        <script>
            jQuery(window).on('load', function () {
                var $preloader = jQuery('#page-preloader');
                $preloader.delay(350).fadeOut('slow');
            });
        </script>
    </body>
</html>