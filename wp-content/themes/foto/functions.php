<?php

/* ========================================================
 * CUSTOM POST TYPE (TESTMONIALS)
  ======================================================== */
load_template(get_template_directory() . '/CustomPost.php');
$custom_post = new foto\WP\Classes\CustomPost('demo');
$custom_post->make('testmonial', 'Testmonial', 'Testmonials');
/* * ****************************************************** */


/* ========================================================
 * THEME SELECTOR
  ======================================================== */

function theme_selector() {
    add_meta_box('theme_selector', 'Select Theme', 'set_theme', 'page', 'normal', 'core');
}

add_action('add_meta_boxes', 'theme_selector');

function set_theme($post) {
    wp_nonce_field('save_theme', 'meta_box_nonce');

    $value = get_post_meta($post->ID, 'current_color_theme', true);

    $dark = null;
    $white = null;

    if (!empty($value) && esc_attr($value) == 'dark') {
        $dark = 'selected';
        $white = "";
    } elseif (!empty($value) && esc_attr($value) == 'white') {
        $white = 'selected';
        $dark = "";
    }

    $string = "<select name='selected_theme'>";
    $string .= "<option value = 'white' " . $white . ">Светлая</option>";
    $string .= "<option value = 'dark' " . $dark . ">Темная</option>";
    $string .= "</select>";
    echo $string;
}

function save_theme($post_id) {
    if (!isset($_POST['selected_theme'])) {
        return;
    }
    $my_data = sanitize_text_field($_POST['selected_theme']);
    update_post_meta($post_id, 'current_color_theme', $my_data);
}

add_action('save_post', 'save_theme');
/* * ****************************************************** */


/* ========================================================
 * ADD TESTMONIAL FORM RUS
  ======================================================== */

function add_form() {
    add_meta_box('add_form', 'Add Testmonial Form -- Russian', 'set_shortcode', 'page', 'normal', 'core');
}

add_action('add_meta_boxes', 'add_form');

function set_shortcode($post) {
    wp_nonce_field('save_form', 'meta_box_nonce');

    $value = get_post_meta($post->ID, 'current_shortcode', true);

    $string = "<label for='shortcode'>Введите сюда ID формы. (Русский)</label>";
    $string .= "<input type='text' id='shortcode' name='shortcode' value=" . esc_attr($value) . ">";
    echo $string;
}

function save_form($post_id) {
//    if (!isset($_POST['meta_box_nonce'])) {
//        return;
//    }
//    if (!wp_verify_nonce($_POST['meta_box_nonce'], 'save_form')) {
//        return;
//    }
//    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
//        return;
//    }
//    if (isset($_POST['post_type']) && 'page' == $_POST['post_type']) {
//        if (!current_user_can('edit_page', $post_id)) {
//            return;
//        }
//    } else {
//        if (!current_user_can('edit_post', $post_id)) {
//            return;
//        }
//    }
    if (!isset($_POST['shortcode'])) {
        return;
    }
    $my_data = sanitize_text_field($_POST['shortcode']);
    update_post_meta($post_id, 'current_shortcode', $my_data);
}

add_action('save_post', 'save_form');
/* * ****************************************************** */


/* ========================================================
 * ADD TESTMONIAL FORM ENG
  ======================================================== */

function add_form2() {
    add_meta_box('add_form2', 'Add Testmonial Form -- English', 'set_shortcode2', 'page', 'normal', 'core');
}

add_action('add_meta_boxes', 'add_form2');

function set_shortcode2($post) {
    wp_nonce_field('save_form2', 'meta_box_nonce');

    $value = get_post_meta($post->ID, 'current_shortcode2', true);

    $string = "<label for='shortcode2'>Введите сюда ID формы. (Английский)</label>";
    $string .= "<input type='text' id='shortcode2' name='shortcode2' value=" . esc_attr($value) . ">";
    echo $string;
}

function save_form2($post_id) {
//    if (!isset($_POST['meta_box_nonce'])) {
//        return;
//    }
//    if (!wp_verify_nonce($_POST['meta_box_nonce'], 'save_form2')) {
//        return;
//    }
//    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
//        return;
//    }
//    if (isset($_POST['post_type']) && 'page' == $_POST['post_type']) {
//        if (!current_user_can('edit_page', $post_id)) {
//            return;
//        }
//    } else {
//        if (!current_user_can('edit_post', $post_id)) {
//            return;
//        }
//    }
    if (!isset($_POST['shortcode2'])) {
        return;
    }
    $my_data2 = sanitize_text_field($_POST['shortcode2']);
    update_post_meta($post_id, 'current_shortcode2', $my_data2);
}

add_action('save_post', 'save_form2');
/* * ****************************************************** */


/* ========================================================
 * REGISTER MENU
  ======================================================== */

function register_my_menu() {
    register_nav_menus(array(
        'header-menu' => 'Меню WHITE',
        'header-menu-2' => 'Меню DARK',
    ));
}

add_action('init', 'register_my_menu');

/* ========================================================
 * CREATING EMPTY GALLERY PAGE ONCE THEME INSTALLED
  ======================================================== */
if (isset($_GET['activated']) && is_admin()) {

    $new_page_title = 'Галерея';
    $new_page_content = '';
    $new_page_template = '';
    $page_check = get_page_by_title($new_page_title);
    $new_page = array(
        'post_type' => 'page',
        'post_title' => $new_page_title,
        'post_content' => $new_page_content,
        'post_status' => 'publish',
        'post_author' => 1,
    );
    if (!isset($page_check->ID)) {
        $new_page_id = wp_insert_post($new_page);
        if (!empty($new_page_template)) {
            update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
        }
    }
}

// Language Select Code for non-Widget users
function language_flags_menu() {
    if (!function_exists("qtrans_getSortedLanguages")) {
        // plugin qTranslate must be active
        echo '<!-- Please activate the qTranslate plugin -->';
        return;
    }
    global $q_config;
    if (is_404()) {
        $url = get_option('home');
    } else {
        $url = '';
    }
    $languages = qtrans_getSortedLanguages(true);
    foreach ($languages as $language) {
        $classes = array('language', 'lang-' . $language);
        // add an extra style class to the active language
        if ($language == $q_config['language']) {
            $classes[] = 'active';
        }
        echo '<div class="' . implode(' ', $classes) . '"><a href="' . qtrans_convertURL($url, $language) . '"';
        echo ' hreflang="' . $language . '" title="' . $q_config['language_name'][$language] . '">';
        echo $q_config['language_name'][$language]. '</a></div>' . "\n";
    }
}
