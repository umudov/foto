<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php bloginfo('name'); ?></title>
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/main.css">
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/social-icons-v2.css">
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>

        <?php wp_head(); ?>
    </head>

    <body <?php body_class($class . ' ' . get_post_meta($post->ID, 'current_color_theme')[0]); ?>>
        <div id="page-preloader"></div>
        <div id="wrapper">
            <?php if (is_front_page() == false) : ?>
                <div class="languages">
                    <?php language_flags_menu(); ?>
                </div>
                <div class="header">
                    <div class="logo">
                        <a href="/foto"><?php bloginfo('name'); ?><sup>©</sup></a>
                    </div>

                    <div class="links">
                        <a class="light" href="/foto/gallery-light">art photo</a>
                        <a class="dark" href="/foto/gallery-dark">art design</a>
                    </div>
                    <div class="menu-wrapper light">
                        <?php wp_nav_menu(array('theme_location' => 'header-menu')); ?>
                    </div>
                    <div class="menu-wrapper dark">
                        <?php wp_nav_menu(array('theme_location' => 'header-menu-2')); ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="page-wrap">
                <hr>
                <div class="copy-block">
                    <?php if (function_exists("qtrans_getSortedLanguages")): ?>
                        <?php if (qtrans_getLanguage() == "en"): ?>
                            © copy and distribute images and information from this site without permission of the author and models - is prohibited.
                        <?php elseif (qtrans_getLanguage() == "ru"):
                            ?>
                            © копировать и распространять изображения и информацию с этого сайта без разрешения автора и моделей - запрещено.
                        <?php endif; ?>
                    <?php endif; ?>
                </div>