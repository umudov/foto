<?php get_header(); ?>
<div class="right-panel">
    <div class="scroll-top-btn">
        <span class="ico-open-svg">
            <svg height="15" width="25" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 15">
                <path fill-rule="evenodd" d="M 0.01 14.05 C 0.01 14.05 1.06 15 1.06 15 C 1.06 15 12.51 2.13 12.51 2.13 C 12.51 2.13 23.95 15 23.95 15 C 23.95 15 25 14.05 25 14.05 C 25 14.05 12.51 0.01 12.51 0.01 C 12.51 0.01 0.01 14.05 0.01 14.05 Z"></path>
            </svg>
        </span>
    </div>
</div>

<?php
while (have_posts()) : the_post();
    the_content(); // выводим контент
endwhile;
?>

<?php get_footer(); ?>