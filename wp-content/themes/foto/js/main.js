jQuery(document).ready(function ($) {
    // back 2 top
    var $scrolltop = $('.scroll-top-btn');
    jQuery(document).scroll(function ($) {
        if (window.pageYOffset > 400) {
            $scrolltop.fadeIn(500);
        } else {
            $scrolltop.fadeOut(500);
        }
    });
    
    jQuery('.add-testmonial .toggle-form').click(function () {
       $('.add-testmonial .form-wrapper').toggle(400);
    });

    jQuery($scrolltop).click(function ($) {
        jQuery("html, body").animate({scrollTop: 0}, "slow");
    });
    
    var $sub_menu = $('.sub-menu');
    var $menu = $('.menu-item-has-children');

    jQuery($menu).hover(function() {
       $sub_menu.stop(true,true).slideDown(300); 
    });
    jQuery($menu).mouseleave(function() {
       $sub_menu.stop(true,true).slideUp(300); 
    });

    var $left;
    var $top;
    var $yPos;

    $( document ).on( "mousemove", function( event ) {
        $left = event.pageX;
        $top = event.pageY;
    });

    $(document).on('touchstart', 'img', function(e) {
        $yPos = e.originalEvent.touches[0].pageY;
    });

    jQuery(document).on('contextmenu', function(e) {
        if(!jQuery(e.target).is('.page-wrap img')) {
            return false;
        }
        if(jQuery(window).width() > 768) {
            jQuery('.copy-block').css('cssText','top:' + $top + 'px; left:' + $left + 'px;').stop(true,true).fadeIn(400).delay(4000).fadeOut(200);
        } else {
            jQuery('.copy-block').css('cssText','top:' + $yPos + 'px; left: 0px;').stop(true,true).fadeIn(400).delay(4000).fadeOut(200);
        }
        e.preventDefault();
    });
});