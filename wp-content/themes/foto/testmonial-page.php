<?php
/**
 * Template Name: Page of Testmonials
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_header(); ?>
<div class="right-panel">
    <div class="scroll-top-btn">
        <span class="ico-open-svg">
            <svg height="15" width="25" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 15">
                <path fill-rule="evenodd" d="M 0.01 14.05 C 0.01 14.05 1.06 15 1.06 15 C 1.06 15 12.51 2.13 12.51 2.13 C 12.51 2.13 23.95 15 23.95 15 C 23.95 15 25 14.05 25 14.05 C 25 14.05 12.51 0.01 12.51 0.01 C 12.51 0.01 0.01 14.05 0.01 14.05 Z"></path>
            </svg>
        </span>
    </div>
</div>
<?php
$type = 'testmonial';
$args = array(
    'post_type' => $type,
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'caller_get_posts' => 1,
    'order' => 'ASC'
);

$my_query = new WP_Query($args);
?> 

<?php
$feed = "Give feedback";
$formID = "";
if (function_exists("qtrans_getSortedLanguages")) {
    if (qtrans_getLanguage() == "ru") {
        $feed = "Оставить Отзыв";
        $formID = get_post_meta($post->ID, 'current_shortcode')[0];
    } else {
        $formID = get_post_meta($post->ID, 'current_shortcode2')[0];
    }
}
?>
<div class = "add-testmonial clearfix">
    <div class = "col-md-12">
        <div class = "send-testmonial">
            <a class = "toggle-form" href = "#"><?php echo $feed; ?></a>
        </div>
        <div class = "form-wrapper">
            <?php echo do_shortcode('[contact-form-7 id="' . $formID . '"]');
            ?>
        </div>
    </div>
</div>

<?php
if ($my_query->have_posts()) {
    while ($my_query->have_posts()) : $my_query->the_post();
        ?>
        <div class="testmonial" style="overflow:hidden;">
            <div class="col-md-4 col-xs-12 col-lg-4 col-sm-4">
                <div class="image-wrapper">
                    <?php echo get_the_post_thumbnail($post->ID); ?>
                </div>
            </div>
            <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                <div class="text-wrapper">
                    <div class="title"><?php echo $post->post_title; ?></div>
                    <div class="text"><?php echo $post->post_content; ?></div>
                </div>
            </div>
        </div>
        <?php
    endwhile;
}
$my_query = null;
wp_reset_query();  // Restore global post data stomped by the_post().
?>
<?php get_footer(); ?>